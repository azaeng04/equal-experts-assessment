FROM cypress/included:7.4.0

RUN apt-get update
RUN apt-get install chromium -y
RUN mkdir /app/
COPY . /app/
WORKDIR /app/
RUN yarn install --no-optional

ENTRYPOINT []
