import * as validBookingData from '../fixtures/validBookingData.json';
import * as validBookingNoDepositData from '../fixtures/validBookingNoDepositData.json';

import moment from 'moment';

const oneMonthLater = moment.utc().add(1, 'month').format('YYYY-MM-DD');
const twoMonthsLater = moment.utc().add(2, 'month').format('YYYY-MM-DD');

describe('Hotel Booking', () => {
  beforeEach(() => {
    cy.cleanUp();
  });

  it('can add a customer booking with valid data', () => {
    cy.visit('/');
    cy.get('#firstname').type('James');
    cy.get('#lastname').type('Taylor');
    cy.get('#totalprice').type('2000');
    cy.get('#depositpaid').select('true');
    cy.get('#checkin').type(oneMonthLater);
    cy.get('#checkout').type(twoMonthsLater);
    cy.get('input[value=" Save "]').click();

    cy.percySnapshot();
  });

  it('can add a customer booking without making a deposit', () => {
    cy.visit('/');
    cy.get('#firstname').type('James');
    cy.get('#lastname').type('Taylor');
    cy.get('#totalprice').type('2000');
    cy.get('#depositpaid').select('false');
    cy.get('#checkin').type(oneMonthLater);
    cy.get('#checkout').type(twoMonthsLater);
    cy.get('input[value=" Save "]').click();

    cy.percySnapshot();
  });

  it('can add a customer booking with valid data - API', () => {
    cy.request({
      method: 'POST',
      url: '/booking',
      body: validBookingData,
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.be.eql(200);
      expect(response.body.booking).to.deep.eq(validBookingData.default);
    });
  });

  it('can add a customer booking without making a deposit - API', () => {
    cy.request({
      method: 'POST',
      url: '/booking',
      body: validBookingNoDepositData,
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.be.eql(200);
      expect(response.body.booking).to.deep.eq(
        validBookingNoDepositData.default
      );
    });
  });

  it('can fetch a booking by booking ID', () => {
    cy.request({
      method: 'POST',
      url: '/booking',
      body: validBookingData,
      failOnStatusCode: false,
    }).then((response) => {
      cy.request({
        method: 'GET',
        url: `/booking/${response.body.bookingid}`,
        failOnStatusCode: false,
      }).then((res) => {
        expect(res.status).to.be.eql(200);
        expect(res.body).to.deep.eq(validBookingData.default);
      });
    });
  });

  it('can fetch all bookings', () => {
    cy.request({
      method: 'POST',
      url: '/booking',
      body: validBookingData,
      failOnStatusCode: false,
    }).then((response) => {
      cy.request({
        method: 'GET',
        url: '/booking',
        failOnStatusCode: false,
      }).then((res) => {
        Cypress._.forEach(res.body, (value, key) => {
          expect(value.bookingid).to.be.eql(response.body.bookingid);
        });
        expect(res.body.length).to.be.greaterThan(0);
        expect(res.status).to.be.eql(200);
      });
    });
  });

  it('can delete a booking by booking Id', () => {
    cy.request({
      method: 'POST',
      url: '/booking',
      body: validBookingData,
      failOnStatusCode: false,
    }).then((response) => {
      cy.request({
        auth: {
          username: 'admin',
          password: 'password123',
        },
        method: 'DELETE',
        url: `/booking/${response.body.bookingid}`,
        failOnStatusCode: false,
      }).then((res) => {
        expect(res.status).to.be.eql(201);
        expect(res.body).to.be.eql('Created');
        cy.request({
          method: 'GET',
          url: `/booking/${response.body.bookingid}`,
          failOnStatusCode: false,
        }).then((res) => {
          expect(res.status).to.be.eql(404);
          expect(res.body).to.be.eq('Not Found');
        });
      });
    });
  });

  it('cannot add a customer booking with no data', () => {
    cy.intercept('POST', '/booking').as('booking');
    cy.visit('/');

    cy.get('input[value=" Save "]').click();
    cy.wait('@booking').then((booking) => {
      expect(booking.response?.statusCode).to.be.eql(500);
      expect(booking.response?.body).to.be.eql('Internal Server Error');
    });
  });

  it('can delete a booking', () => {
    cy.request({
      method: 'POST',
      url: '/booking',
      body: validBookingData,
    });
    cy.visit('/');
    cy.get('input[value="Delete"]').click();

    cy.percySnapshot();
  });

  it('cannot add a customer without a firstname ', () => {
    const newValidBookingData = Cypress._.clone(validBookingData);
    delete newValidBookingData.firstname;

    cy.request({
      method: 'POST',
      url: '/booking',
      body: newValidBookingData,
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.be.eql(500);
      expect(response.body).to.be.eql('Internal Server Error');
    });
  });

  it('cannot add a customer without a surname', () => {
    const newValidBookingData = Cypress._.clone(validBookingData);
    delete newValidBookingData.lastname;

    cy.request({
      method: 'POST',
      url: '/booking',
      body: newValidBookingData,
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.be.eql(500);
      expect(response.body).to.be.eql('Internal Server Error');
    });
  });

  it('cannot add customer without a price', () => {
    const newValidBookingData = Cypress._.clone(validBookingData);
    delete newValidBookingData.totalprice;

    cy.request({
      method: 'POST',
      url: '/booking',
      body: newValidBookingData,
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.be.eql(500);
      expect(response.body).to.be.eql('Internal Server Error');
    });
  });

  it('cannot add a customer without a deposit', () => {
    const newValidBookingData = Cypress._.clone(validBookingData);
    delete newValidBookingData.depositpaid;

    cy.request({
      method: 'POST',
      url: '/booking',
      body: newValidBookingData,
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.be.eql(500);
      expect(response.body).to.be.eql('Internal Server Error');
    });
  });

  it('cannot add a customer without a check-in', () => {
    const newValidBookingData = Cypress._.clone(validBookingData);
    delete newValidBookingData.bookingdates.checkin;

    cy.request({
      method: 'POST',
      url: '/booking',
      body: newValidBookingData,
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.be.eql(500);
      expect(response.body).to.be.eql('Internal Server Error');
    });
  });

  it('cannot add a customer without a check-out', () => {
    const newValidBookingData = Cypress._.clone(validBookingData);
    delete newValidBookingData.bookingdates.checkout;

    cy.request({
      method: 'POST',
      url: '/booking',
      body: newValidBookingData,
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.be.eql(500);
      expect(response.body).to.be.eql('Internal Server Error');
    });
  });
});
