// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false;
});

Cypress.Commands.add('cleanUp', () => {
  cy.request({
    method: 'GET',
    url: '/booking',
  }).then((bookings) => {
    Cypress._.forEach(bookings.body, (value, key) => {
      if (bookings.body.length > 0) {
        cy.request({
          auth: {
            username: 'admin',
            password: 'password123',
          },
          method: 'DELETE',
          url: `/booking/${value.bookingid}`,
        });
      }
    });
  });
});
