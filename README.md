# Equal Experts Assessment

This holds the test automation for the Equal Experts assessment


**NB: This has been executed successfully on a Macbook. Though I don't have access to a Windows or Linux machine. It should work fine if docker is not used.**

# Getting started

This setup is for running it locally on your given system

1.  Clone the repository by running: `git clone https://gitlab.com/azaeng04/equal-experts-assessment.git`
2.  Install the `node_modules` with either `yarn` or `npm`: `yarn install` or `npm install`
3.  Verify that Cypress can run on your system: `yarn cy:verify` or `npm run cy:verify`
4.  Open the Cypress runner: `yarn cy:open` or `npm run cy:open` and select a test suite to execute

**NB: Since this is running locally without Docker you would require atleast one common browser such as Chrome and/or Firefox**

# Executing with Docker

This setup is for running it locally on your given system with `docker compose` or `docker-compose`

1. Clone the repository by running: `git clone https://gitlab.com/azaeng04/equal-experts-assessment.git`
2. Execute the following command to build the docker image: `docker build -t cypress .`
3. Execute the following command to execute the tests in either Chrome or Firefox:

   `docker compose run --rm e2e yarn test:chrome` or
   `docker-compose run --rm e2e yarn test:chrome`

   OR in Firefox

   `docker compose run --rm e2e yarn test:firefox` or
   `docker-compose run --rm e2e yarn test:firefox` // Does not seem to work as it is a known issue by Cypress: https://github.com/cypress-io/cypress/issues/8610

# Test Results

Test results can be viewed on the Cypress Dashboard here: https://dashboard.cypress.io/projects/9cmkn8

Percy is a visual regression tool that compares images to a baseline. Those results can be viewed here: https://percy.io/107cf4a1/equal-experts-assessment